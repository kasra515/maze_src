#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <algorithm>
#include <iostream>
#include <termios.h>

using namespace std;

#define TERMINATE "DISCONNECT"
#define QUIT "BYE"
#define SUCCESS "SUCCESS"
#define FAILURE "FAILURE"

#define BUF_SIZE 1024

char getch() {
        char buf = 0;
        struct termios old = {0};
        if (tcgetattr(0, &old) < 0)
                perror("tcsetattr()");
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN] = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
                perror("tcsetattr ICANON");
        if (read(0, &buf, 1) < 0)
                perror ("read()");
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;
        if (tcsetattr(0, TCSADRAIN, &old) < 0)
                perror ("tcsetattr ~ICANON");
        return (buf);
}


int setupSocket(char* serverName, unsigned long serverPort) 
{
	struct sockaddr_in sockadd;
	struct hostent *hp;
	int sd;

	if ((hp = gethostbyname(serverName)) == NULL) {
		perror("gethostbyname");
		return -1;
	}

	sockadd.sin_family = AF_INET;
	sockadd.sin_addr.s_addr = ((struct in_addr *)(hp->h_addr))->s_addr;
	sockadd.sin_port = htons(serverPort);

	if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		return -1;
	}

	if(connect(sd, (struct sockaddr*) &sockadd, sizeof(sockaddr)) == -1) {
		perror("connect");
		return -1;
	}

	return sd;
}

std::string toUpper(const std::string &str) {
	std::string newStr(str);
	std::transform(newStr.begin(), newStr.end(), newStr.begin(), ::toupper);
	return newStr;
}

int main(int argc, char* argv[]) 
{
	if(argc != 3) {
		fprintf(stderr, "%s <server name> <server port>\n", argv[0]);
		return 1;	
	}

	char* serverName = argv[1];
	unsigned long serverPort = strtol(argv[2], NULL, 10);

	int socket_descriptor = setupSocket(serverName, serverPort);
	if(socket_descriptor < 0) { fprintf(stderr, "could not create socket descriptor\n"); return 1; }

	//receive HI
	char buffer2[BUF_SIZE];
	if(recv(socket_descriptor, buffer2, BUF_SIZE, 0) < 1) {
			fprintf(stderr, "recv error or got no bytes\n");
			return 1 ;
	}
	printf("received: %s\n", buffer2);

	while(true) 
	{
		system("clear");
		char buffer[BUF_SIZE];

		cout << "Press any key to stop the robot...\n";

		char input[2];
		input[0] = getch();
		input[1] = '\0';
		std::string command(input);
		std::string upperCaseCommand = toUpper(command);

		if(upperCaseCommand.find(QUIT) != std::string::npos) break;
		send(socket_descriptor, command.c_str(), 2, 0);

		cout << "\nStop sent to the robot, please type \"start\" to start again\n";

		string command2;
		do
		{
			if(fgets(buffer, BUF_SIZE, stdin) == NULL) break;
			command2 = std::string(buffer);
			send(socket_descriptor, command2.c_str(), sizeof(command2.c_str()), 0);
		}
		while(command2.compare("start\n") != 0);
		
	}

	send(socket_descriptor, TERMINATE, sizeof(TERMINATE), 0);

	close(socket_descriptor);

	return 0;
}
