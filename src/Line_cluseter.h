#ifndef CLUSTER_H
#define	CLUSTER_H


class Line_cluseter
{
public:

	Line_cluseter()
	{

		angle_mean = -1000;
		angle_sum = 0.0;
	}

	void Add(Line& line, int index)
	{
		if(angle_sum == -1000)
			angle_mean = 0;
		lines.push_back(line);
		line_indexes.push_back(index);
		if(lines.size() == 1)
			angle_mean = 0;
		angle_sum += line.GetAngle();
		angle_mean = angle_sum / (double)lines.size();
	}

	//return number of lines in the cluster 2
	int size()
	{
		return lines.size();
	}

	void clear()
	{
		lines.clear();
		line_indexes.clear();
		//angle_mean = 0; keep the mean 

		angle_sum = 0;
	}

	Line& element_at(int i)
	{
		return lines.at(i);
	}

	//variables

	vector<Line> lines;
	//to keep track of line indexes in the first line vector
	vector<int> line_indexes;
	double angle_mean;
	double angle_sum ;

};

#endif