#include <geometry_msgs/PoseWithCovariance.h>
#include <boost/thread/thread.hpp>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <termios.h>
#include <signal.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/poll.h>
#include "ros/ros.h"
#include <tf2_msgs/TFMessage.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <cstdlib>
#include "State.h"
#include "Quaternion.h"
#include "local_planner.h"

#include <tf/transform_listener.h>

#define KEYCODE_W 0x77
#define KEYCODE_A 0x61
#define KEYCODE_S 0x73
#define KEYCODE_D 0x64

#define KEYCODE_A_CAP 0x41
#define KEYCODE_D_CAP 0x44
#define KEYCODE_S_CAP 0x53
#define KEYCODE_W_CAP 0x57

struct Position
{
    int x,y;
};
struct Orientation
{
    float w, z;
};
class Maze_Local;
Maze_Local* tbk;
struct termios cooked, raw;
int kfd = 0;
bool done;

class Maze_Local
{
    private:

        geometry_msgs::Twist cmdvel_;
        geometry_msgs::PoseStamped goal_location_;
        ros::NodeHandle n_;
        ros::NodeHandle receiver_pos_handle;
        ros::Publisher pub_;
        ros::Publisher goal_pos_pub_;
        ros::Subscriber pose_sub;
        State goal;

        //start and current state of the robot (changes during the program)
        State *start;

        Local_Planner lp;
        double heading_error_epsilon;
        double distance_tolerance;
        
        //set to 1 when reach the goal
        int reached_goal;

        //length of each grid cell in the maze (here it's 2 feet)
        double step_length;

        //for transforming the map to base_link to get the robot's position
        tf::TransformListener listener;

        //this variable is used in the controller 
        double beta;


    public:
        Maze_Local()
        {
            goal_pos_pub_ = n_.advertise<geometry_msgs::PoseStamped>("/rviz_goal", 10);
            pub_ = n_.advertise<geometry_msgs::Twist>("/cmd_vel", 100);

            //use pose_sub when want to use amcl & comment it out when using gmap
            //pose_sub = n_.subscribe<geometry_msgs::PoseWithCovarianceStamped>("/amcl_pose", 10, &Maze_Local::UpdatePosition, this);

            //current position
            //hardcode the start position
            start = new State();
            start->x = 0;
            start->y = 0;
            start->v = 0;
            start->w = 0;
            start->quaternion = Quaternion(0, 0, 1, 0.0);

            goal.x = 0;
            goal.y = 0;

            step_length = 0.6096;
            lp = Local_Planner(*start, step_length);
            heading_error_epsilon = 5.0;
            distance_tolerance = 0.04;
            reached_goal = 0;
        }
        
        ~Maze_Local() 
        { 

        }


        /*
          This method rotates the robot for 360 degrees
          so the map will be updated
        */
        void Scan()
        {
            string open_cells[4];
            
            double start_heading = start->quaternion.Yaw_in_degrees();
            int hits_180 = 0;//set to 1 when is rotated 180 degrees
            int heading_difference_epsilon = 10;

            ros::Rate loop_rate(100);
            bool estop_set = false;
            while(ros::ok)
            {
                cmdvel_.linear.x = 0;
                cmdvel_.angular.z = 0.7;

                 string estop_value="";
                if(ros::param::get("estop", estop_value)) 
                {
                    estop_set = true;
                }

                //comment SetPosition out when using amcl
                SetPosition();

                if(estop_set && estop_value.compare("true") == 0)
                {
                    cout << "estop set to true!" << endl;
                    cmdvel_.linear.x = 0;
                    cmdvel_.angular.z = 0;
                }
                else
                {

                    double current_state_heading = start->quaternion.Yaw_in_degrees();
                    double heading_difference = fabs(current_state_heading - start_heading);

                    //cout << "Heading difference: " <<  heading_difference << endl;

                    if(fabs(heading_difference - 180) < heading_difference_epsilon)
                    {
                        hits_180 = 1;

                    }
                    if(hits_180 == 1 && fabs(heading_difference) < heading_difference_epsilon)
                    {
                        cmdvel_.angular.z = 0.0;
                        pub_.publish(cmdvel_);
                        //calculate open grids
                        break;
                    }
                }

                pub_.publish(cmdvel_);


                ros::spinOnce();

                loop_rate.sleep();
            }
        }

        void SetRvizGoal()
        {
            goal_location_.header.frame_id="map";

            goal_location_.pose.position.x = goal.x;
            goal_location_.pose.position.y = goal.y;


            Quaternion q;
            double beta_rad = q.RadianToDegree(beta);
            q = Quaternion(0, 0, 1, beta_rad);
            goal_location_.pose.orientation.w = q.w;
            goal_location_.pose.orientation.z = q.z;


        }


        void MoveTowardGoal(State &goal_location, double _beta)
        {
            beta = _beta;
            goal.x = goal_location.x;
            goal.y = goal_location.y;

            SetRvizGoal();
            moveLoop();
        }


        /*
        ros::ok loop runs inside this function 
        */
        void moveLoop()
        {
            ros::Rate loop_rate(100);

            //this variable is used to make sure we are heading to goal before start moving toward it
            int heading_toward_goal = 0;
            bool estop_set = false;

            while(ros::ok)
            {
                cmdvel_.linear.x = 0;
                cmdvel_.angular.z = 0;

                
                string estop_value="";
                if(ros::param::get("estop", estop_value)) 
                {
                    estop_set = true;
                }

                //comment SetPosition out when using amcl
                SetPosition();
               
                if(estop_set && estop_value.compare("true") == 0)
                {
                    cout << "estop set to true!" << endl;
                    cmdvel_.linear.x = 0;
                    cmdvel_.angular.z = 0;
                }
                else if(!reached_goal)
                {
                    //cout << "Current :" << " x: " << start->x << ", " << " y: " << start->y << " w: " << start->quaternion.w << " z: " << start->quaternion.z << endl;
                    //cout<< "Goal: " << goal.x << ", " <<goal.y << endl;
                    
                    double distance = lp.Goal_Distance(*start, goal);

                    double heading_error =  lp.Headding_Angle_error(*start, goal);
                    //cout << "Heading Error: " << heading_error << endl ;
                    heading_error = (M_PI * heading_error) / 180.0;

                    double kd, k_alpha, k_beta;

                    kd = 1.1;
                    k_alpha = 0.6;
                    k_beta = 0.9;


                    //set translational velocity to 0 if heading error is big
                    if(heading_toward_goal == 0 && (heading_error > 0.05 || heading_error < -0.05))
                    {
                        kd = 0.05;
                        k_beta = 0;
                    }
                    else if(heading_toward_goal == 0 && (heading_error <= 0.05 || heading_error >= -0.05))
                    {
                        heading_toward_goal = 1;
                    }

                    if(heading_toward_goal == 1 && (heading_error > 0.34 || heading_error < -0.34))
                    {
                        kd = 0.05;
                        k_beta = 0;
                    }

                    double beta_parameter;
                    double current_heading = start->quaternion.Yaw_in_Radian();

                    if(beta == 3.14)
                    {
                        if(current_heading < 0)
                        {
                            beta_parameter = (-1 * beta) - current_heading;
                        }
                        else
                        {
                            beta_parameter = beta - current_heading;
                        }
                    }
                    else
                        beta_parameter = beta - current_heading;

                    double v = kd * distance;
                    double w = k_alpha * heading_error;//  + k_beta * beta_parameter;

                    if(distance > distance_tolerance)
                    {
                        //cout << "distance: " << distance << endl << endl;
                        cmdvel_.linear.x = v;
                        cmdvel_.angular.z = w;
                    }
                    else
                    {
                        cmdvel_.linear.x = 0;
                        cmdvel_.angular.z = 0;
                        reached_goal = 1;
                    }
                }
                else
                {
                    cmdvel_.linear.x = 0;
                    cmdvel_.angular.z = 0;
                    cout << "!!!!!!!!!!! ----------------- Reached goal ------------------------" << endl;
                    cout << endl << endl << endl;
                    reached_goal = 0;
                    break;
                }


                pub_.publish(cmdvel_);
                goal_pos_pub_.publish(goal_location_);


                ros::spinOnce();

                loop_rate.sleep();
            }
        }


        //This function is used when getting the position from amcl
        void UpdatePosition(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& pose)
        {
            
         
            float x = pose->pose.pose.position.x;
            float y = pose->pose.pose.position.y;
            float z = pose->pose.pose.orientation.z;
            float w = pose->pose.pose.orientation.w;

            start->quaternion.Set(w, 0, 0, z);
            start->x = x;
            start->y = y;

            //ROS_INFO("Position received");
        }
        


        /*
        This function is called in ros::ok loop to get the transforms
        from map to base link from the gmapping and estimate robot's position
        http://answers.ros.org/question/39250/getting-the-position-and-the-pose-of-robot-using-tf_listener/
        http://answers.ros.org/question/41892/position-and-orientation-of-the-robot/
        */
        void SetPosition()
        {
            tf::StampedTransform transform;
            try
            {
                listener.lookupTransform("/map", "/base_link",  ros::Time(0), transform);
            }
            catch (tf::TransformException &ex) {
                ROS_ERROR("%s",ex.what());
                ros::Duration(1.0).sleep();
                return;
            }
            start->x = transform.getOrigin().x();
            start->y = transform.getOrigin().y();

            double yaw = tf::getYaw(transform.getRotation());

            double w = cos(yaw / 2.0);
            double z = sin(yaw / 2.0);

            start->quaternion.Set(w, 0, 0, z);
        }



        void stopRobot()
        {
            cmdvel_.linear.x = 0.0;
            cmdvel_.angular.z = 0.0;
            pub_.publish(cmdvel_);
        }
};


