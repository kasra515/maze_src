#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <string>
#include <sstream>
#include <algorithm>
#include <ros/ros.h>
#include "maze_local.cpp"
#include "Line.h"
#include "maze_coordinate.cpp"
#include <nav_msgs/GridCells.h>
#include "maze_matrix.h"

using namespace std;

#define SHUTDOWN "SHUTDOWN"
#define INIT "INIT"
#define NEWCONNECTION "HI\n"
#define CLOSECONNECTION "BYE\n"
#define LOOK "SCAN"
#define MOVE "MOVE"
#define SUCCESS "OK\n"
#define FAILURE "INVALID\n"
#define FAILURE2 "HUH\n"

#define NORTHCMD "NORTH"
#define SOUTHCMD "SOUTH"
#define EASTCMD "EAST"
#define WESTCMD "WEST"
#define GOAL "GOAL"

#define LISTENING_DEPTH 1
#define BUF_SIZE 1024


State Current_state;
vector<Line> map_lines;
double maze_coordinate_rotation;
//geometry_msgs::PoseStamped goal_location_;
State goal_location;
State rotated_goal_location;
State _translated_goal_location;
double step_length = 0.6096;
double maze_rotation_angle;
double x_shift, y_shift;
int program_started = 1;

int open_directions_string_size;
char *scan_open_directions;
enum Direction { 	NORTH 	= 0,
					EAST 	= 1,
					SOUTH 	= 2,
					WEST 	= 3,
};

//keep track of the open cells
MazeMatrix _maze_matrix;

//maze coordinate current location
//these variables change by 1 and represent the cell of the maze that robot is in it
int location_in_maze_x, location_in_maze_y;

Direction orientation = NORTH;

bool scan_done;


int setupSocket(unsigned long serverPort) {
	struct sockaddr_in sock;
	int socket_descriptor;

	if ((socket_descriptor = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		return -1;
	}

	sock.sin_family = AF_INET;
	sock.sin_addr.s_addr = INADDR_ANY;
	sock.sin_port = htons(serverPort);

	if(bind(socket_descriptor, (struct sockaddr*)&sock, sizeof(sock)) == -1) {
		perror("bind");
		return -1;
	}

	if (listen(socket_descriptor, LISTENING_DEPTH) == -1) {
		perror("listen");
		return -1;
	}
	return socket_descriptor;
}

std::string toUpper(const std::string &str) {
	std::string newStr(str);
	std::transform(newStr.begin(), newStr.end(), newStr.begin(), ::toupper);
	return newStr;
}


// Returns 1 if the lines intersect, otherwise 0. In addition, if the lines 
// intersect the intersection point may be stored in the floats i_x and i_y.
//http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
int get_line_intersection(double p0_x, double p0_y, double p1_x, double p1_y, 
    double p2_x, double p2_y, double p3_x, double p3_y, double *i_x, double *i_y)
{
    double s02_x, s02_y, s10_x, s10_y, s32_x, s32_y, s_numer, t_numer, denom, t;
    s10_x = p1_x - p0_x;
    s10_y = p1_y - p0_y;
    s32_x = p3_x - p2_x;
    s32_y = p3_y - p2_y;

    denom = s10_x * s32_y - s32_x * s10_y;
    if (denom == 0)
        return 0; // Collinear
    bool denomPositive = denom > 0;

    s02_x = p0_x - p2_x;
    s02_y = p0_y - p2_y;
    s_numer = s10_x * s02_y - s10_y * s02_x;
    if ((s_numer < 0) == denomPositive)
        return 0; // No collision

    t_numer = s32_x * s02_y - s32_y * s02_x;
    if ((t_numer < 0) == denomPositive)
        return 0; // No collision

    if (((s_numer > denom) == denomPositive) || ((t_numer > denom) == denomPositive))
        return 0; // No collision
    // Collision detected
    t = t_numer / denom;
    if (i_x != NULL)
        *i_x = p0_x + (t * s10_x);
    if (i_y != NULL)
        *i_y = p0_y + (t * s10_y);

    return 1;
}
int LineIntersect (Line &line1, Line &line2)
{
	double p0_x = line1.start.x;
	double p0_y = line1.start.y;

	double p1_x = line1.end.x;
	double p1_y = line1.end.y;

	double p2_x = line2.start.x;
	double p2_y = line2.start.y;

	double p3_x = line2.end.x;
	double p3_y = line2.end.y;

	double intersection_x, intersection_y;

	if(get_line_intersection(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, &intersection_x, &intersection_y) == 0)
		return 0;
	else
		return 1;
}

int IntersectMapLines(State& state , Point &destination)
{
	///////!!!!!!!!!!!!!!!!! change this to step lenght
	double lines_distance = 0.05;
	//create 3 lines between current location and the destination location to make sure we catch walls if there is a small error in the map
	Line line1, line2, line3;

	Point current_location;
	//current location equals to goal location's here because this function is called after scan's function which don't change the goal
	current_location.x = state.x;
	current_location.y = state.y;

	line1.start.x = current_location.x + lines_distance;
	line1.start.y = current_location.y + lines_distance;
	line1.end.x = destination.x + lines_distance;
	line1.end.y = destination.y + lines_distance;
	
	line2.start.x = current_location.x;
	line2.start.y = current_location.y;
	line2.end.x = destination.x;
	line2.end.y = destination.y;

	line3.start.x = current_location.x - lines_distance;
	line3.start.y = current_location.y - lines_distance;
	line3.end.x = destination.x - lines_distance;
	line3.end.y = destination.y - lines_distance;

	//line2.Print();

	int size = map_lines.size();
	for(int i = 0; i < size; i++)
	{
		if(LineIntersect(line1, map_lines.at(i)) != 0 ||
			LineIntersect(line2, map_lines.at(i)) != 0||
			LineIntersect(line3, map_lines.at(i)) != 0 )
		{
			return 1;
		}
	}
	return 0;
}



void UpdateStepLength()
{
	string param_value="";
    if(ros::param::get("step_length", param_value)) 
    {
    	step_length = strtof(param_value.c_str(), NULL);
    	ROS_INFO("received step_length: %f", step_length);
    }
	else ROS_WARN("step_length not set, using: %f", step_length);
}

void UpdateCorrdinateShift()
{
	string param_value="";
    if(ros::param::get("x_shift", param_value)) 
    {
    	x_shift = strtof(param_value.c_str(), NULL);
    	ROS_INFO("received x_shift: %f", x_shift);
    }
	else ROS_WARN("x_shift not set, using: %f", 0);

	if(ros::param::get("y_shift", param_value)) 
    {
    	y_shift = strtof(param_value.c_str(), NULL);
    	ROS_INFO("received y_shift: %f", y_shift);
    }
	else ROS_WARN("y_shift not set, using: %f", 0);

	if(ros::param::get("maze_rotation_angle", param_value)) 
    {
    	maze_rotation_angle = strtof(param_value.c_str(), NULL);
    	ROS_INFO("default rotation angle: %f", maze_rotation_angle);
    }
    cout << "received rotation angle: " << maze_rotation_angle << endl;
}


/*
This function returns the open neibour cells
          it returns an array of string 
          this array elements can be NORTH, SOUTH, WEST, EAST which are the open neibour cells
          open cell is set to number of open neibour cells
*/
vector<string> OpenCells(State& state, Point &north_goal, Point &south_goal, Point &west_goal, Point &east_goal)
{
	std::vector<string> open_cells;
	cout << "map lines size: " << map_lines.size() << endl;
	cout << "current location: " << state.x << ", " << state.y << endl;

	if(IntersectMapLines(state, north_goal) == 0)
	{
		open_cells.push_back("NORTH");
	}
	if(IntersectMapLines(state, south_goal) == 0)
	{
		open_cells.push_back("SOUTH");
	}
	if(IntersectMapLines(state, east_goal) == 0)
	{
		open_cells.push_back("EAST");
	}
	if(IntersectMapLines(state, west_goal) == 0)
	{
		open_cells.push_back("WEST");
	}

	return open_cells;

}

vector<string> OpenCellsforState(int location_x, int location_y)
{
	UpdateStepLength();
	UpdateCorrdinateShift();

	State robot_state, robot_state_in_map;
	robot_state.x = location_x * step_length;
	robot_state.y = location_y * step_length;

	Maze_coordinate::MoveToMazeCoordinate(robot_state, robot_state_in_map);

	State north_in_maze, south_in_maze, east_in_maze, west_in_maze;

	north_in_maze.x = (location_x + 1) * step_length;
	north_in_maze.y = (location_y) * step_length ;

	south_in_maze.x = (location_x - 1) * step_length;
	south_in_maze.y = (location_y) * step_length;

	east_in_maze.x = (location_x) * step_length;
	east_in_maze.y = (location_y - 1) * step_length;

	west_in_maze.x = (location_x) * step_length;
	west_in_maze.y = (location_y + 1) * step_length;


	State _north_in_map, _south_in_map, _east_in_map, _west_in_map;

	Maze_coordinate::MoveToMazeCoordinate(north_in_maze, _north_in_map);
	Maze_coordinate::MoveToMazeCoordinate(south_in_maze, _south_in_map);
	Maze_coordinate::MoveToMazeCoordinate(east_in_maze, _east_in_map);
	Maze_coordinate::MoveToMazeCoordinate(west_in_maze, _west_in_map);

	Point north_in_map(_north_in_map);
	Point south_in_map(_south_in_map);
	Point east_in_map(_east_in_map);
	Point west_in_map(_west_in_map);

	vector<string> open_cells = OpenCells(robot_state_in_map, north_in_map, south_in_map, west_in_map, east_in_map);

	return open_cells;
}


bool lookCommand(int socket_descriptor, Maze_Local& ml)
{
	ROS_INFO("SCAN COMMAND");

	//check to see whether we have been in that cell before
	vector<string> open_cells = _maze_matrix.Get(location_in_maze_x, location_in_maze_y);
	if(open_cells.size() == 0)
	{
		ml.Scan();
		open_cells = OpenCellsforState(location_in_maze_x, location_in_maze_y);
		_maze_matrix.Set(location_in_maze_x, location_in_maze_y, open_cells);

		//scan the open cell to avoid extra scans
		for(int i = 0; i < open_cells.size(); i++)
		{
			int future_x, future_y;
			if(open_cells.at(i).compare("NORTH") == 0)
			{
				future_x = location_in_maze_x + 1;
				future_y = location_in_maze_y;
			}
			if(open_cells.at(i).compare("SOUTH") == 0)
			{
				future_x = location_in_maze_x - 1;
				future_y = location_in_maze_y;
			}
			if(open_cells.at(i).compare("EAST") == 0)
			{
				future_x = location_in_maze_x ;
				future_y = location_in_maze_y - 1;
			}
			if(open_cells.at(i).compare("WEST") == 0)
			{
				future_x = location_in_maze_x ;
				future_y = location_in_maze_y + 1;
			}
			if(_maze_matrix.Get(future_x, future_y).size() > 0)
				continue;
			vector<string> future_open_cells = OpenCellsforState(future_x, future_y);
			_maze_matrix.Set(future_x, future_y, future_open_cells);
		}
	}
	else
		open_cells = _maze_matrix.Get(location_in_maze_x, location_in_maze_y);

	//the scan mtehod of ml just rotates the robot for 360 degrees
	

	//look at the map_lines and return the open cells in an array e.g {NORTH, EAST}
	/* for now it rotates 360 each times and then call this function,
	 but ideally it should check to see whether we know the wall around robot first and rotate if necessary */

	open_directions_string_size = 0;
	string open_directions = "";
	for(int i = 0; i < open_cells.size(); i++)
	{	
		open_directions += open_cells.at(i);
		open_directions_string_size += (open_cells.at(i).length() + 1);//one for space
		
		if( i != open_cells.size() - 1)
			open_directions += " ";
	}
	open_directions += "\n";
	
	scan_open_directions = (char*)open_directions.c_str();
	cout << "Sent open directions to the client: " << scan_open_directions << "\n" ;
	scan_done = true;

	return true;
}

bool moveCommand(int socket_descriptor, char* _command, Maze_Local& ml)
{
	string command = std::string(_command);

	//check to see if we can go to that direction
	//vector<string> open_cells = OpenCellsforState();

	//beta variable is used for detection the direction of the goal location in rviz
	double beta;

	vector<string> open_cells = _maze_matrix.Get(location_in_maze_x, location_in_maze_y);

	if(open_cells.size() > 0 && std::find(open_cells.begin(), open_cells.end(), command) == open_cells.end())
		return false;

	if(command == "NORTH")
    {	
    	location_in_maze_x += 1;
        beta = 0;
    }
    else if(command == "SOUTH")
    {
    	location_in_maze_x -= 1;
        beta = 3.14;
    }
    else if(command =="EAST")
    {
    	location_in_maze_y -= 1;
        beta = -1.57;
    }
    else if(command =="WEST")
    {
    	location_in_maze_y += 1;
        beta = 1.57;
    }

	goal_location.x = location_in_maze_x * step_length;    
	goal_location.y = location_in_maze_y * step_length;   

	Maze_coordinate::MoveToMazeCoordinate(goal_location, _translated_goal_location);
	cout << "_translated_goal_location: (" << _translated_goal_location.x << ", " << _translated_goal_location.y << ")" << endl;
	ml.MoveTowardGoal(_translated_goal_location, beta);

	return true;
}

void UpdateMapLines(const visualization_msgs::Marker::ConstPtr& msg)
{
	int size = msg->points.size();
	//cout << "number of received lines: !!!!!!!!!!!!!!!!!!!!!!!!!" << size << endl;
	map_lines.resize(size);
	map_lines.clear();

	Line l;
	for(int i = 0; i < size; i++)
	{
		if(i % 2 == 0)
		{
			l.start.x = msg->points[i].x;
			l.start.y = msg->points[i].y;
		}
		else
		{
			l.end.x = msg->points[i].x;
			l.end.y = msg->points[i].y;
			map_lines.push_back(l);
		}
	}	
}



int main(int argc, char* argv[]) 
{
	scan_done = false;
	location_in_maze_x = location_in_maze_y = 0;
	//heading to x+ direction
	Current_state.x = 0;
	Current_state.y = 0;
	maze_coordinate_rotation = 0;

	ros::init(argc, argv, "maze_runner");
	Maze_Local ml;
	ros::NodeHandle n;

	ros::Subscriber map_lines_sub = n.subscribe<visualization_msgs::Marker>("visualization_marker", 10, UpdateMapLines);

	unsigned long serverPort = 22222;
	double dxy = 0.5;
	double dr = M_PI / 2;
	std::string filename = "maze.dat";

	

	std::string param;

	if(n.getParam("port", param)) serverPort = strtol(param.c_str(), NULL, 10);
	else ROS_WARN("port not set, using: %u", serverPort);

	if(n.getParam("dxy_discr", param)) dxy = strtof(param.c_str(), NULL);
	else ROS_WARN("dxy_discr not set, using: %f", dxy);

	if(n.getParam("dr_discr", param)) dr = strtof(param.c_str(), NULL);
	else ROS_WARN("dr_discr not set, using: %f", dr);

	if(n.getParam("init_orien", param)) orientation = NORTH;
	else ROS_WARN("init_orien not set, using: NORTH");

	if(n.getParam("graph_file", param)) filename = param;
	else ROS_WARN("graph_file not set, using: %s", filename.c_str());

	ros::param::set("goal_color", "yellow");

	int socket_descriptor = setupSocket(serverPort);
	if(socket_descriptor < 0) { fprintf(stderr, "could not create socket descriptor\n"); return 1; }

	char buffer[BUF_SIZE];

	while(true) {
		int current_sd = -1;

		ros::param::set("reached_goal", "no");
		string color_param_value="";
	    if(ros::param::get("goal_color", color_param_value)) 
	    {
	    	if(color_param_value.compare ("red") == 0 )
	    	{
	    		ros::param::set("goal_color", "yellow");
	    		ROS_INFO("Goal Color: Yellow");
	    	}
	    	else
	    	{
	    		ros::param::set("goal_color", "red");
	    		ROS_INFO("Goal Color: Red");
	    	}
	    }
		

		if((current_sd = accept(socket_descriptor, NULL, 0)) < 0) {
			perror("accept");
			break;
		}
		else {
			ROS_INFO("CONNECTED");
			send(current_sd, NEWCONNECTION, sizeof(NEWCONNECTION), 0);

			while(true) {
				
				if(recv(current_sd, buffer, BUF_SIZE, 0) < 1) {
					fprintf(stderr, "recv error or got no bytes\n");
					break;
				}
				if(program_started)
				{	
					ml.Scan();
					program_started = 0;
				}
				std::string command(buffer);
				std::string upperCaseCommand = toUpper(command);

				ROS_INFO("received String: %s\n", upperCaseCommand.c_str());
				if(upperCaseCommand.find(SHUTDOWN) != std::string::npos) {
					ROS_INFO("shutdown request %s\n", upperCaseCommand.c_str());
					close(current_sd);
					goto shutdown;
				}
				else if(upperCaseCommand.find(CLOSECONNECTION) != std::string::npos) {
					ROS_INFO("disconnect request %s\n", upperCaseCommand.c_str());
					break;
				}
				else if(upperCaseCommand.find(LOOK) != std::string::npos) {
					//if(!lookCommand(current_sd, graph, rw)) {
					if(!lookCommand(current_sd, ml)) {
						ROS_ERROR("command failed: %s", buffer);
						break;
					}
					send(current_sd, scan_open_directions, open_directions_string_size  , 0);
				}
				else if(upperCaseCommand.find(NORTHCMD) != std::string::npos) { //check to see if invalid direction
					if(!moveCommand(current_sd, NORTHCMD, ml)) {
						ROS_ERROR("invalid movement: %s", buffer);
						//break;
					}
					string reached_goal_str;
					if(ros::param::get("reached_goal", reached_goal_str)) 
		    		{
		    			if(reached_goal_str.compare("yes") == 0)
		    			{
		    				ROS_INFO("Reached the final goal......\n");
		    				send(current_sd, GOAL, sizeof(GOAL), 0);
		    				break;
		    			}
		    		}
					send(current_sd, SUCCESS, sizeof(SUCCESS), 0);
				}
				else if(upperCaseCommand.find(SOUTHCMD) != std::string::npos) {
					if(!moveCommand(current_sd, SOUTHCMD, ml)) {
						ROS_ERROR("invalid movement: %s", buffer);
						//break;
					}
					string reached_goal_str;
					if(ros::param::get("reached_goal", reached_goal_str)) 
		    		{
		    			if(reached_goal_str.compare("yes") == 0)
		    			{
		    				ROS_INFO("Reached the final goal......\n");
		    				send(current_sd, GOAL, sizeof(GOAL), 0);
		    				break;
		    			}
		    		}
					send(current_sd, SUCCESS, sizeof(SUCCESS), 0);
				}
				else if(upperCaseCommand.find(EASTCMD) != std::string::npos) {
					if(!moveCommand(current_sd, EASTCMD,ml) ){
						ROS_ERROR("invalid movement: %s", buffer);
						//break;
					}
					string reached_goal_str;
					if(ros::param::get("reached_goal", reached_goal_str)) 
		    		{
		    			if(reached_goal_str.compare("yes") == 0)
		    			{
		    				ROS_INFO("Reached the final goal......\n");
		    				send(current_sd, GOAL, sizeof(GOAL), 0);
		    				break;
		    			}
		    		}
					send(current_sd, SUCCESS, sizeof(SUCCESS), 0);
				}
				else if(upperCaseCommand.find(WESTCMD) != std::string::npos) {
					if(!moveCommand(current_sd, WESTCMD, ml)) {
						ROS_ERROR("invalid movement: %s", buffer);
						//break;
					}
					string reached_goal_str;
					if(ros::param::get("reached_goal", reached_goal_str)) 
		    		{
		    			if(reached_goal_str.compare("yes") == 0)
		    			{
		    				ROS_INFO("Reached the final goal......\n");
		    				send(current_sd, GOAL, sizeof(GOAL), 0);
		    				break;
		    			}
		    		}
					send(current_sd, SUCCESS, sizeof(SUCCESS), 0);
				}
				else {
					ROS_ERROR("unrecognized command: %s", buffer);
					send(current_sd, FAILURE2, sizeof(FAILURE2), 0);
					break;
				}

				
			}
			close(current_sd);
		}
	}

shutdown:
	ROS_WARN("maze runner closing");
	close(socket_descriptor);

	return 0;
}
