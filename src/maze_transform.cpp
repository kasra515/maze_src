


#include "ros/ros.h"
#include "std_msgs/String.h"
#include <std_msgs/Float64.h>
#include <iostream>
#include "Line.h"
#include <vector>
#include "maze_coordinate.cpp"
#include <visualization_msgs/Marker.h>
#include <tf/transform_listener.h>
#include <tf/tf.h>
#include "State.h"

using namespace std;


//for transforming the map to base_link to get the robot's position
tf::TransformListener* listener = NULL; 
//current state of the robot (changes during the program)
State *current;

vector<Line> map_lines;
Maze_coordinate mc;
ros::Publisher cell_publisher;

//use local information in the maze transformation
//we copy the lines that are around the current location of the robot over and over to make their weight heighvier
int local;

void Fill_map_lines(const visualization_msgs::Marker::ConstPtr& msg)
{
	double distance_threshold = 0.6;//change it to the step_length*2
	double theta;

	string param_value="";
 	if(ros::param::get("step_length", param_value)) 
	    	distance_threshold = strtof(param_value.c_str(), NULL);
	else ROS_WARN("maze_rotation_angle not set, using: %f", distance_threshold);

	
	int size = msg->points.size();

	map_lines.resize(size);
	map_lines.clear();

	Line l;
	for(int i = 0; i < size; i++)
	{
		if(i % 2 == 0)
		{
			l.start.x = msg->points[i].x;
			l.start.y = msg->points[i].y;
		}
		else
		{
			l.end.x = msg->points[i].x;
			l.end.y = msg->points[i].y;

			Point robot_current;
			robot_current.x = current->x;
			robot_current.y = current->y;

			if(local && l.shortestDistanceToPoint(robot_current) < distance_threshold)
			{
				//copy it over and over
				for(int j = 0; j < 20; j++)
				{
					map_lines.push_back(l);
				}
			}
			else
			{
				map_lines.push_back(l);
			}
		}
	}
}

 /*
This function is called to get the transforms
from map to base link from the gmapping and estimate robot's position
*/
void SetCurrentPosition()
{
    tf::StampedTransform transform;

    try
    {
        listener->lookupTransform("/map", "/base_link",  ros::Time(0), transform);
    }
    catch (tf::TransformException &ex) 
    {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
        return;
    }

    current->x = transform.getOrigin().x();
    current->y = transform.getOrigin().y();

    double yaw = tf::getYaw(transform.getRotation());

    double w = cos(yaw / 2.0);
    double z = sin(yaw / 2.0);

    current->quaternion.Set(w, 0, 0, z);
    cout << "\ncurrent state: " << current->x <<", " << current->y << " w, z :" << current->quaternion.w << ", " <<current->quaternion.z << endl;
}

void map_lines_update(const visualization_msgs::Marker::ConstPtr& msg)
{
	SetCurrentPosition();
	Fill_map_lines(msg);
	int lines_count = map_lines.size();
	ROS_INFO("received lines count: %d", lines_count);
	
    //calibrate the maze coordinate
    //calculate the rotation
    std_msgs::Float64 rotate_angle = mc.CalibrateAngle(*current, map_lines);
    std::ostringstream strs;
	strs << rotate_angle.data;
	std::string str = strs.str();
	ros::param::set("maze_rotation_angle", str);

	//calculate the step length
    double step_length = mc.StepLength();
    std::ostringstream step_stream;
	step_stream << step_length;
	std::string step_str = step_stream.str();
	ros::param::set("step_length", step_str);

	//calculate the maze coordinate transformation
	double x_shift, y_shift;
    mc.MazeCorrdinateTransformation((double)rotate_angle.data, step_length, map_lines, x_shift, y_shift);
    cout << "(x_shift, y_shift) = (" << x_shift << ", " << y_shift << ")" << endl;
    
    std::ostringstream x_shift_stream;
	x_shift_stream << x_shift;
	std::string x_shift_str = x_shift_stream.str();

	std::ostringstream y_shift_stream;
	y_shift_stream << y_shift;
	std::string y_shift_str = y_shift_stream.str();

    ros::param::set("x_shift", x_shift_str);
    ros::param::set("y_shift", y_shift_str);

    //send the grid cell to rviz
    visualization_msgs::Marker cellsList;
    mc.SendGridCellToRviz(step_length, cellsList);
    cell_publisher.publish(cellsList);
}

int main(int argc, char **argv)
{

	ros::init(argc, argv, "maze_transform");
	listener = new (tf::TransformListener);
	current = new State();
	ros::NodeHandle n;

	local = 0;
	if(argv[1] != NULL && strcmp(argv[1], "local") == 0 )
		local = 1;

	if(local == 1)
		ROS_INFO("Using local information for the coordinate transformation");
	else
		ROS_INFO("Using global information for the coordinate transformation");
	
	ros::Subscriber lines_sub = n.subscribe("visualization_marker", 10, map_lines_update);
	cell_publisher = n.advertise<visualization_msgs::Marker>("grid_cell", 10);

	ros::spin();

	return 0;
}
