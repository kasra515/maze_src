#ifndef POINT_H
#define	POINT_H

#include "State.h"

class Point
{
public:
	//default constructor
	Point()
	{
		x = 0;
		y = 0;
	}
	Point (State& s)
	{
		x = s.x; 
		y = s.y;
	}

	//copy constructor
	Point (const Point &p)
	{
		x = p.x;
		y = p.y;
	}
	//assignment operator
    Point& operator=(const Point& other)
    {
    	x = other.x;
    	y = other.y;
    }
    void setX(double X) { x = X; }
    void setY(double Y) { y = Y; }

    double getX() const { return x; }
    double getY() const { return y; }

	double x;
	double y;
};

#endif