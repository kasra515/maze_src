#ifndef UTIL_H
#define UTIL_H

#include "Line.h"
#include "point.h"
class Util
{

public:
	static double Distance(Point &p1, Point &p2)
	{
	    double distance = pow(p2.x - p1.x, 2.0) + pow(p2.y - p1.y, 2.0);
	    distance = sqrt(distance);

	    return distance;
	}


	static double dot(Point &c1, Point &c2)
	{
	    return (c1.x * c2.x + c1.y * c2.y);
	}
	 
	static double norm(Point &c1)
	{
	    return sqrt(dot(c1, c1));
	}

	//===================================================================
	//#define d(u,v)      norm(u-v)          // distance = norm of difference
    static float d(Point &u, Point &v)
    {
    	Point diff;
    	diff.x = u.x - v.x;
    	diff.y = u.y - v.y;

    	return norm(diff);
    }

	//http://geomalgorithms.com/a02-_lines.html
	// dist_Point_to_Segment(): get the distance of a point to a segment
	//     Input:  a Point P and a Segment S (in any dimension)
	//     Return: the shortest distance from P to S
	static float
	dist_Point_to_Segment( Point &P, Line &S)
	{
		Point v ;
		v.x = S.end.x - S.start.x;
		v.y = S.end.y - S.start.y;
		Point w;
		w.x = P.x - S.start.x;
		w.y = P.y - S.start.y;

		double c1 = dot(w,v);
		if ( c1 <= 0 )
		  return d(P, S.start);

		double c2 = dot(v,v);
		if ( c2 <= c1 )
		  return d(P, S.end);

		double b = c1 / c2;
		Point Pb;
		Pb.x = S.start.x + b * v.x;
		Pb.y = S.start.y + b * v.y;
		return d(P, Pb);
	}




	static double getShortestDistance(Line &line1, Line &line2)
	{
	    double EPS = 0.00000001;
	 
	    Point delta21 ;
	    delta21.x = line1.end.x - line1.start.x;
	    delta21.y = line1.end.y - line1.start.y;
	 
	    Point delta41;
	    delta41.x = line2.end.x - line2.start.x;
	    delta41.y = line2.end.y - line2.start.y;
	 
	    Point delta13;
	    delta13.x = line1.start.x - line2.start.x;
	    delta13.y = line1.start.y - line2.start.y;
	 
	    double a = dot(delta21, delta21);
	    double b = dot(delta21, delta41);
	    double c = dot(delta41, delta41);
	    double d = dot(delta21, delta13);
	    double e = dot(delta41, delta13);
	    double D = a * c - b * b;
	 
	    double sc, sN, sD = D;
	    double tc, tN, tD = D;
	 
	    if (D < EPS)
	    {
	        sN = 0.0;
	        sD = 1.0;
	        tN = e;
	        tD = c;
	    }
	    else
	    {
	        sN = (b * e - c * d);
	        tN = (a * e - b * d);
	        if (sN < 0.0)
	        {
	            sN = 0.0;
	            tN = e;
	            tD = c;
	        }
	        else if (sN > sD)
	        {
	            sN = sD;
	            tN = e + b;
	            tD = c;
	        }
	    }
	 
	    if (tN < 0.0)
	    {
	        tN = 0.0;
	 
	        if (-d < 0.0)
	            sN = 0.0;
	        else if (-d > a)
	            sN = sD;
	        else
	        {
	            sN = -d;
	            sD = a;
	        }
	    }
	    else if (tN > tD)
	    {
	        tN = tD;
	        if ((-d + b) < 0.0)
	            sN = 0;
	        else if ((-d + b) > a)
	            sN = sD;
	        else
	        {
	            sN = (-d + b);
	            sD = a;
	        }
	    }
	 
	    if (abs(sN) < EPS) sc = 0.0;
	    else sc = sN / sD;
	    if (abs(tN) < EPS) tc = 0.0;
	    else tc = tN / tD;
	 
	    Point dP;
	    dP.x = delta13.x + (sc * delta21.x) - (tc * delta41.x);
	    dP.y = delta13.y + (sc * delta21.y) - (tc * delta41.y);
	 
	    return sqrt(dot(dP, dP));
	}
};



#endif