/* 
 * File:   local_planner.h
 * Author: kasra
 *
 * Created on November 24, 2014, 2:44 PM
 */

#ifndef LOCAL_PLANNER_H
#define	LOCAL_PLANNER_H

#include "State.h"

class Local_Planner
{
private:
    State current_state;

    //length of each step or square in the maze
    double step_length;
    
public:
    //default contructor
    Local_Planner()
    {
        
    }
    
    Local_Planner(State &_state, double step)
    {
        //step_lenght is 2 feet
        step_length = step; 
        current_state = _state;
    }
    void Move_to(State &goal);
    int Is_Heading_to_Goal();
    
    //returns the heading angle difference between goal and current state
    //double Headding_Angle_error(State &goal);
    /*
    if heading error < 0 => turn left
    else
        turn right
    */
    double Headding_Angle_error(State& start, State &goal);

    double Goal_Distance (State &s1, State &s2);
    /*
    This function is called when we need to turn toward the goal
    it gets to double variables and fills them with the proper angular (and linear) velocity
    */
    void turn_toward_goal(double heading_error, double &linear_x, double& angular_z);

    void move_toward_goal(double heading_error, double distance, double &linear_x, double &angular_z);
};


#endif	/* LOCAL_PLANNER_H */

