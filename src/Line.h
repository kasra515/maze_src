#ifndef LINE_H
#define	LINE_H

#include <iostream>
#include "point.h"
#include <math.h>
#include <string>
#include <sstream>


using namespace std;

class Line
{
public:
	//default constrcurtor
	Line()
	{
		start = Point();
		end = Point();
	}
	Line (Point &_start, Point &_end)
	{
		start = _start;
		end = _end;
	}

	//copy constructor
	Line (const Line &l)
	{
		start = Point();
		end = Point();
		start = l.start;
		end = l.end;
	}
	//assignment operator
    Line& operator=(const Line& other)
    {
    	start = Point();
		end = Point();
    	start = other.start;
    	end = other.end;
    }

    void Print()
    {
    	cout << "( " << start.x << ", " << start.y << ") -> ( " << end.x << ", " << end.y << ")\n";
    }


    double GetAngle()
    {
    	double xDiff = end.x - start.x;
    	double yDiff = end.y - start.y;
    	double result = atan2 (yDiff,xDiff) * 180 / M_PI;
    	//if(result < 0)
    		//result += 360.0;
    	return result;
    }


	double shortestDistanceToPoint(Point &p) 
	{
	    
	    //used the eq in http://geomalgorithms.com/a02-_lines.html
	    double numerator = fabs( (start.y - end.y) * p.x + (end.x - start.x) * p.y + (start.x * end.y - end.x * start.y) );
	    double denominator = sqrt (pow(end.x - start.x , 2.0) + pow(end.y - start.y , 2.0));

	    double distance = numerator / denominator;

	    return distance;
	}



    /*
	the shortest of the distance between point A and line segment CD, B and CD, C and AB or D and AB. 
	*/
	double DistanceToAnotherLineSegments(Line &b)
	{
	    double min_distance = std::numeric_limits<double>::max();

	    double distance1 = shortestDistanceToPoint( b.start ) ;
	    if(distance1 < min_distance)
	        min_distance = distance1;

	    double distance2 = shortestDistanceToPoint( b.end ) ;
	    if(distance2 < min_distance)
	        min_distance = distance2;

	    

	    return min_distance;
	}

	//length of the line
	double Length()
	{
		double distance = pow(end.x - start.x, 2.0) + pow(end.y - start.y, 2.0);
	    distance = sqrt(distance);

	    return distance;
	}

	string DoubleToString(double num)
	{
		std::ostringstream ss;
	    ss << num;
	    return ss.str();
	}
	string GetString()
	{
		string s = "";
		s += ( "(" + DoubleToString(start.x) + ", " + DoubleToString(start.y) + ")->(" + DoubleToString(end.x) + ", " + DoubleToString(end.y )+ ")\n" ) ;

		return s;

	}

    //variables
	Point start;
	Point end;
};

#endif