/*
This class is responsible for converting the locations in the maze to the locations in a matrix
locations in the maze can have negative indexes for example (0-2, -2) is a valid location in the maze
Here we convert these locations to a matrix with positive indexes
in the matrix top left is (0,0), top right is (N, 0), bottom right is (N, N)
This class is used in the scan function, it keeps track of the open cells
if we have been in a location before we don't do the scan, because we already have open cells
*/
#ifndef MAZEMATRIX_H
#define	MAZEMATRIX_H

using namespace std;
#include <iostream>
#include <string>
#include <vector>

#define N 17

struct OpenCells
{
	vector<string> open_cells;
};

class MazeMatrix
{
private:
	OpenCells cells[N][N];

public:
	void Set(int i, int j, vector<string> open_cells)
	{
		int row = i + (N / 2);
		int col = (N / 2) - j;
		cells[row][col].open_cells = open_cells;
	}

	vector<string> Get(int i, int j)
	{
		int row = i + (N / 2);
		int col = (N / 2) - j;
		return cells[row][col].open_cells;
	}
};

#endif
