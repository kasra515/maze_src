/* 
 * File:   State.h
 * Author: kasra
 *
 * Created on November 24, 2014, 2:37 PM
 */

#ifndef STATE_H
#define	STATE_H

#include "Quaternion.h"


class State
{
public:
        
    ///////////////////////////// variables ///////////////////////////
    
    // position, we don't have z in this project
    double x, y; 
    Quaternion quaternion;
    
    //translational velocity
    double v;
    //angular velocity
    double w;
    
    
    ///////////////////////////// Methods ///////////////////////////
    //default constructor
    State()
    {
        x = 0;
        y = 0;
        v = 0;
        w = 0;
    }
    
    //copy constructor
    State(State &_state)
    {
        x = _state.x;
        y = _state.y;
        v = _state.v;
        w = _state.w;
        quaternion = _state.quaternion;
    }
    
    //assignment operator
    State& operator=(const State& other)
    {
        x = other.x;
        y = other.y;
        v = other.v;
        w = other.w;
        quaternion = other.quaternion;
    }

    
};

#endif	/* STATE_H */

