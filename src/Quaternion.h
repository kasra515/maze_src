/* 
 * File:   Quaternion.h
 * Author: kasra
 *
 * Created on November 25, 2014, 3:04 PM
 */

#ifndef QUATERNION_H
#define QUATERNION_H

#include <math.h>
#include <iostream>

using namespace std;

class Quaternion
{
public:
    Quaternion()
    {
        w = x = y = z = 0;
    }
    
    //copy constructor
    Quaternion (const Quaternion& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        w = other.w;
    }
    
    //assignment operator
    Quaternion& operator=(const Quaternion& other)
    {
        x = other.x;
        y = other.y;
        z = other.z;
        w = other.w;
    }
    
    /*
    x, y, z can be 0, 1 indicating the rotation axis
    theta is rotation degree
    */
    Quaternion(double _x, double _y, double _z, double theta)
    {
        theta = DegreeToRadian(theta);

        w = cos(theta / 2.0);
        x = _x * sin ( theta / 2.0);
        y = _y * sin ( theta / 2.0);
        z = _z * sin ( theta / 2.0);
    }
    
    void Print()
    {
        cout << "(" << w <<", " << x << ", " << y << ", " << z << ")\n";
    }

    //set the parameters manually
    void Set(double _w, double _x, double _y, double _z)
    {
        w = _w;
        x = _x;
        y = _y;
        z = _z;
    }
    
    double Yaw_in_Radian()
    {
        double yaw;
        if(z >= 0.0)
            yaw = acos(w) * 2;
        else
            yaw = -acos(w) * 2;
   
        return yaw;
    }
    
    double Yaw_in_degrees()
    {
        double degree = RadianToDegree(Yaw_in_Radian());
        /*if(degree < 0)
            degree += 360;*/
        return degree;
    }
    
    double DegreeToRadian(double theta)
    {
        double radian = (M_PI/180.0) * theta ;
        return radian;
    }
    
    double RadianToDegree(double radians)
    {
        double theta =  (180.0 / M_PI) * radians;
        return theta;
    }
    

    double w, x, y, z;
private:
    
};

#endif  /* QUATERNION_H */

