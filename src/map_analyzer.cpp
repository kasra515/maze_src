#include "ros/ros.h"
#include "nav_msgs/OccupancyGrid.h"
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include "point.h"
#include "Line.h"
#include <math.h>
#include <visualization_msgs/Marker.h>
#include "linreg.cpp"
#include "Util.cpp"



using namespace std;

ros::Publisher marker_pub;
ros::Publisher rotation_pub;


const double resolution = 0.01;
//distance from a line that we consider a point to be inline
const float distance_threshold = 0.020;
const int min_line_size = 8;


double Distance(Point &p1, Point &p2)
{
    double distance = pow(p2.x - p1.x, 2.0) + pow(p2.y - p1.y, 2.0);
    distance = sqrt(distance);

    return distance;
}


/*
This function finds one line among all the points using ransac algorithm and then remove those points 
from the reference vector, so we can find other lines
This function is called inside the ransac function
*/

void FindOneLine(vector<Point> &point_vector, vector<Point> &result_line)
{
    int size = point_vector.size();

    //number of ransac iterations
    int k = 300;//for now just selected a number to test

    //we save inliner numbers for each iteration and at the end select the line in iteration with the max inlier numbers
    int *inlier_numbers = new int[k];
    int max_inlier_number = 0;
    int max_inlier_index = -1;

    //store inlier points to remove them after we select the line with max inliers, so next time we won't select this line again
    //inlier points may contain points of two line that are in the same direction so I should filter them out
    Point **inlier_points = new Point*[k];

    //RANSAC iteration
    for(int index = 0; index < k; index++)
    {
        int random_index_1 = rand() % size;
        int random_index_2 = rand() % size;

        Point p1 = point_vector.at(random_index_1);
        Point p2 = point_vector.at(random_index_2);


        int inlier_number = 0;
        double *inliers_x = new double [size];
        double *inliers_y = new double [size];

        for(int i = 0; i < size; i++)
        {

            Point p = point_vector.at(i);
            //perpendicular distance from the point to the line
            //used the formulate in http://www.intmath.com/plane-analytic-geometry/perpendicular-distance-point-line.php
            double distance;
            Line l;
            l.start = p1;
            l.end = p2;

            distance = l.shortestDistanceToPoint(p);

            if(distance < distance_threshold)
            {
                inliers_x[inlier_number] = p.x;
                inliers_y[inlier_number] = p.y;
                inlier_number++;
            }
        }

        inlier_points[index] = new Point[inlier_number];
        for(int i = 0; i < inlier_number; i++)
        {
            inlier_points[index][i].x = inliers_x[i];
            inlier_points[index][i].y = inliers_y[i];
        }
        inlier_numbers[index] = inlier_number;

        if(inlier_number > max_inlier_number)
        {
            max_inlier_number = inlier_number;
            max_inlier_index = index;
        }

        //delete 
        delete inliers_x;
        delete inliers_y;
        inliers_x = inliers_y = 0;
    }

    //filter points to seperate points that are in another line
    int points_in_line_with_max_inliers = inlier_numbers[max_inlier_index] ;

    for(int i = 0; i < points_in_line_with_max_inliers ; i++)
    {

        result_line.push_back(inlier_points[max_inlier_index][i]);
    }
    
    //remove lines from the point vector
    int result_size = result_line.size();

    for(int i = 0; i < result_size; i++)
    {
        for(int j = 0; j < point_vector.size(); j++)
        {
            if(result_line.at(i).x == point_vector.at(j).x &&
                result_line.at(i).y == point_vector.at(j).y)
            {
                point_vector.erase(point_vector.begin()  + j);
            }
        }
    }
    
    delete inlier_numbers;
    //delete inlier points
    for(int i = 0; i < k; i++)
    {
        delete[] inlier_points[i];
    }
    delete []inlier_points;
}



void SendLinesToRViz(vector<Line> &lines)
{
    //send the line to rviz as a path
    visualization_msgs::Marker  line_list;
    line_list.header.frame_id = "/map";
    line_list.header.stamp = ros::Time::now();
    line_list.ns = "points_and_lines";
    line_list.action = visualization_msgs::Marker::ADD;
    line_list.pose.orientation.w = 1.0;

    line_list.id = 0;
    line_list.type = visualization_msgs::Marker::LINE_LIST;

     // LINE_LIST markers use only the x component of scale, for the line width
    line_list.scale.x = 0.02;

    // Line list is red
    line_list.color.g = 1.0;
    line_list.color.a = 1.0;

    int number_of_points = lines.size();

    for(int i = 0; i < number_of_points; i++)
    {
        geometry_msgs::Point p1;
        geometry_msgs::Point p2;

        p1.x = lines.at(i).start.x;
        p1.y = lines.at(i).start.y;

        p2.x = lines.at(i).end.x;
        p2.y = lines.at(i).end.y;

        line_list.points.push_back(p1);
        line_list.points.push_back(p2);
    }

    marker_pub.publish(line_list);
    
}

/*
This function takes a vector of points and specifies line's start and end points
*/
void Find_line_start_and_end(vector<Point> &one_line, Point &start, Point &end)
{
    int size = one_line.size();
    double **distances = new double*[size];

    for(int i = 0; i < size; i++)
    {
        distances[i] = new double[size];
    }
    double max_distance = 0;
    int max_i = 0, max_j = 0;//indexes for max distance

    for(int i = 0; i < size; i++)
    {
        for(int j = i + 1 ; j < size; j++)
        {
            distances[i][j] = Distance(one_line.at(i), one_line.at(j));
            if(distances[i][j] > max_distance)
            {
                max_distance = distances[i][j];
                max_i = i;
                max_j = j;
            }
        }
    }
    //cout << max_i << ", " << max_j << "-- " << max_distance << endl;
    start = one_line.at(max_i);
    end = one_line.at(max_j);

    for(int i = 0; i < size; i++)
    {
        delete[] distances[i];
    }
    delete []distances;
}


/*
This function runs linear regression on set of points and fits a line in that set
http://www.oocities.org/david_swaim/cpp/linreg.htm
*/
void FitALineByRegression(vector<Point> &one_line, Point &start, Point& end,  Line &line_after_regression)
{
    double x_difference = fabs(start.x - end.x);
    double y_difference = fabs(start.y - end.y);

    if(x_difference < 0.1)
    {
        double mean_x = (start.x + end.x) / 2.0;
        line_after_regression.start.x = mean_x;
        line_after_regression.end.x = mean_x;
        line_after_regression.start.y = start.y;
        line_after_regression.end.y = end.y;
    }
    else if(y_difference < 0.1)
    {
        double mean_y = (start.y + end.y) / 2.0;
        line_after_regression.start.y = mean_y;
        line_after_regression.end.y = mean_y; 
        line_after_regression.start.x = start.x;
        line_after_regression.end.x = end.x;
    }
    else
    {
 
        LinearRegression lr2(one_line, one_line.size());  // create with array of points
        //cout << lr2 << endl;
       // cout << "Before Regression: (" <<  start.x << ", " << start.y << ") -- (" << end.x << ", " << end.y << ")" << endl;
        
        //find closest point in the line to the start and end
        //line equasion here is y = a2 + b2x
        double b2, a2;
        b2 = lr2.getB();
        a2 = lr2.getA();

        //line equation in ax + by + c = 0
        double a = -1 * b2;
        double b = 1;
        double c = -1 * a2;

        
        //calculate the closest perpendicular point on the line to the start and end
        //used http://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
        double x_start =  (b * (b * start.x - a * start.y) - (a * c)) / ( (a*a) + (b*b));
        double y_start = (a * ((-1 * b) * start.x + (a * start.y)) - (b * c)) / ((a*a) + (b*b));

        double x_end = (b * (b * end.x - a * end.y) - (a * c)) / ( (a*a) + (b*b));
        double y_end = (a * ((-1 * b) * end.x + (a * end.y)) - (b * c)) / ((a*a) + (b*b));

        //cout << "After Regression: (" <<  x_start << ", " << y_start << ") -- (" << x_end << ", " << y_end << ")" << endl;


        line_after_regression.start.x = x_start;
        line_after_regression.start.y = y_start;
        line_after_regression.end.x = x_end;
        line_after_regression.end.y = y_end;
    }
}

//Compare functions for sorting
bool Point_sort_x (Point i,Point j) 
{ 
    return (i.x < j.x); 
}
bool Point_sort_y (Point i,Point j) 
{ 
    return (i.y < j.y); 
}


/*
This function gets a line as an input, and splits it two different  parallel lines if there is a gap in between(gap in data points)
*/
void SplitParallelLines(vector<Point> &one_line, Point &start, Point &end, vector<vector<Point> > &splitted_lines)
{
    double threshold = 0.20;
    double x_difference = fabs(start.x - end.x);
    double y_difference = fabs(start.y - end.y);
    int size = one_line.size();

    if(x_difference > y_difference)
    {
        //sort by x
        std::sort (one_line.begin(), one_line.end(), Point_sort_x);
        vector<Point> one_splitted_line;

        for(int i = 0; i < size; i++)
        {
            
            if(i == 0 || fabs(one_line.at(i).x - one_line.at(i-1).x < threshold ))
            {
                one_splitted_line.push_back(one_line.at(i));
            }
            else
            {
                if(one_splitted_line.size() > min_line_size)
                {
                    splitted_lines.push_back(one_splitted_line);
                }
                one_splitted_line.clear();
            }
        }
        if(one_splitted_line.size() > min_line_size)
        {
            splitted_lines.push_back(one_splitted_line);
        }
    }
    else
    {
        //sort by y
        std::sort (one_line.begin(), one_line.end(), Point_sort_y);
        
        vector<Point> one_splitted_line;
        for(int i = 0; i < size; i++)
        {
            if(i == 0 || fabs(one_line.at(i).y - one_line.at(i-1).y < threshold ))
            {
                one_splitted_line.push_back(one_line.at(i));
            }
            else
            {
                if(one_splitted_line.size() > min_line_size)
                {
                    splitted_lines.push_back(one_splitted_line);
                }
                one_splitted_line.clear();
            }
        }
        if(one_splitted_line.size() > min_line_size)
        {
            splitted_lines.push_back(one_splitted_line);
        }
    }

}


/*
    This fucntion returns true if two lines are two separate lines that are parallel and the second line starts right after the first line
    The second line shouln't get removed by the RemoveRepitetiveLines function
*/
int TwoLinesAreContinious(Line &a, Line &b)
{
    double angle_threshold = 10;
    Point line_a_middle, line_b_middle;

    line_a_middle.x = (a.start.x + a.end.x) / 2.0;
    line_a_middle.y = (a.start.y + a.end.y) / 2.0;

    line_b_middle.x = (b.start.x + b.end.x) / 2.0;
    line_b_middle.y = (b.start.y + b.end.y) / 2.0;

    Line line_connecting_middles;
    line_connecting_middles.start = line_a_middle;
    line_connecting_middles.end = line_b_middle;

    if  (fabs(line_connecting_middles.GetAngle() - a.GetAngle()) < angle_threshold
            &&
            fabs(line_connecting_middles.GetAngle() - b.GetAngle() )< angle_threshold
        )
    {
        return 1;
    }
    else
        return 0;

}
/*
    This function removes repitetive lines
    it detects the line that are so close to each other,
    then if one of the lines is smaller than the other (maybe subset), it gets removed
*/
void RemoveRepitetiveLines(vector<Line> &lines, vector<Line> &new_lines)
{

    cout << "lines size: " << lines.size() << endl;   
    double min_distance = 0.20;
    double max_allowed_angle_difference = 20;

    int *is_part_of_smaller_line = new int[lines.size()];
    for(int i = 0; i < lines.size(); i++)
    {
        is_part_of_smaller_line[i] = 0;
    }

    for(int i = 0; i < lines.size(); i++)
    {
        if(is_part_of_smaller_line[i])
            continue;
        Line *current_i = &lines.at(i);

        double max_length = current_i->Length();
        int max_index = i;

        for (int j = i+1 ; j < lines.size(); j++)
        {
            Line *current_j = &lines.at(j);

            if( (   Util::getShortestDistance(*current_i, *current_j) < min_distance 
                && 
                    abs(current_i->GetAngle() - current_j->GetAngle()) < max_allowed_angle_difference) 
                &&
                    !TwoLinesAreContinious(*current_i, *current_j)
                ) 
            {   
                if(lines.at(j).Length() > max_length)
                {
                    max_length = lines.at(j).Length();
                    max_index = j;
                }
                else
                    is_part_of_smaller_line[j] = 1;
            }
        }


        new_lines.push_back(lines.at(max_index));
    }
    cout << "new_lines size: " << new_lines.size() << endl;

    delete is_part_of_smaller_line;
}


/*
    RANSAC algorithm
*/
void Ransac(vector<Point> &point_vector)
{
    vector<Line> lines;

    while(point_vector.size() > min_line_size)
    {
        vector<Point> one_line ;
        FindOneLine(point_vector, one_line);
        if(one_line.size() > 0 && one_line.size() > min_line_size)
        {

            Point line_start, line_end;
            Find_line_start_and_end(one_line, line_start, line_end);
            
            //a line may be made of parallel lines and must be splitted, otherwise will report some cells as blocked
            vector<vector<Point> > splitted_lines;
            SplitParallelLines(one_line, line_start, line_end, splitted_lines);
            
            for(int i = 0; i < splitted_lines.size(); i++)
            {
                Line line_after_regression;
                Find_line_start_and_end(splitted_lines.at(i), line_start, line_end);
                //FitALineByRegression(splitted_lines.at(i), line_start, line_end, line_after_regression);

                Line line;
                line.start = line_start;
                line.end = line_end;
                //lines.push_back(line_after_regression);
                lines.push_back(line);
            }
            
        }
    }

    vector<Line> new_lines;
    //RemoveRepitetiveLines(lines, new_lines);
    //SendLinesToRViz(new_lines);
    SendLinesToRViz(lines);
}



void chatterCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
    float resolution = msg->info.resolution;
    int width = msg->info.width;
    int height = msg->info.height;
    double x_orgin = msg->info.origin.position.x;
    double y_orgin = msg->info.origin.position.y;
    double w = msg->info.origin.orientation.w;
    double z = msg->info.origin.orientation.z;
    cout << "----------------------------------\n";
    cout << "Origin: " << x_orgin << ", " << y_orgin << endl;
    cout << "Resolution: " << resolution << endl;
    cout << "width, height: " << width << ", " << height << endl;
    cout << "Z, W: " << z << ", " << w << endl;

    int size = width * height;
    int **data = new int*[width];
    for(int i = 0; i < width; i++)
    {
        data[i] = new int[height];
    }

    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            data[i][j] = msg->data[i*width + j];
        }
    }

    std::vector<Point> point_vector;

    for(int j = 0; j < height; j++)
    {
        for(int i = 0; i < width; i++)
        {
            if(data[i][j] > 85)
            {

                Point p;
                p.x = j * resolution + y_orgin;
                p.y = i * resolution + x_orgin;
                point_vector.push_back(p);
            }
        }
    }

    for(int i=0; i < width; i++)
    {
        delete[] data[i];
    }
    delete []data;
    Ransac(point_vector);
}


int main(int argc, char **argv)
{

  ros::init(argc, argv, "map_analyzer");


  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("map", 10, chatterCallback);

  marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);


  ros::spin();

  return 0;
}
