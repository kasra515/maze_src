#ifndef COORDINATE_H
#define	COORDINATE_H

#include <vector>
#include <iostream>
#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include "State.h"
#include "Util.cpp"
#include "Line_cluseter.h"
#include <visualization_msgs/Marker.h>

using namespace std;



class Maze_coordinate
{



public:
///////////////////////////////// variables

	//translation
	double x, y; 
	//rotatiom
	double theta; 

	Line_cluseter cluster1, cluster2;
	int first_calibration;
	int north_south_cluseter;//can be 1 and 2


	////////////////////////////// Methods //////////////////////////////

	Maze_coordinate()
	{
		x = y = theta = 0;
		first_calibration = 1;
	}


	Maze_coordinate(double _x, double _y, double _theta)
	{
		x = _x;
		y = _y;
		theta = _theta;
		first_calibration = 1;
	}

	

	/*
		this fucntion computes the distances between parallel lines (cluster1 lines and cluster2 lines)
		and if the distance is greater than a distance threshold, then puts it into the vector
		This function is used in calculating step_length
	*/
	vector<double> DistacesOfClustersLines(double &min_distance)
	{
		vector<double> distances;
		double min_distance_threshold = 0.4;


		int cluster1_size = cluster1.size();
		for(int i = 0; i < cluster1_size; i++)
		{
			for(int j = i + 1; j < cluster1_size; j++)
			{
				if(cluster1.element_at(j).Length() < min_distance_threshold)
					continue;
				//double distance = Util::getShortestDistance(cluster1.element_at(i), cluster1.element_at(j));
				double distance = cluster1.element_at(i).DistanceToAnotherLineSegments(cluster1.element_at(j));
				if(distance > min_distance_threshold)
				{
					if(distance < min_distance)
						min_distance = distance;
					distances.push_back(distance);
				}
			}
		}

		int cluster2_size = cluster2.size();
		for(int i = 0; i < cluster2_size; i++)
		{
			for(int j = i + 1; j < cluster2_size; j++)
			{
				//double distance = Util::getShortestDistance(cluster2.element_at(i), cluster2.element_at(j));
				double distance = cluster2.element_at(i).DistanceToAnotherLineSegments(cluster2.element_at(j));
				if(distance > min_distance_threshold)
				{
					if(cluster2.element_at(j).Length() < min_distance_threshold)
						continue;

					if(distance < min_distance)
						min_distance = distance;
					distances.push_back(distance);
				}
			}
		}

		return distances;
	}


	int Min(int num1, int num2)
	{
		int min = std::numeric_limits<int>::max();
		if(num1 < min)
			min = num1;
		if(num2 < min)
			min = num2;

		return min;
	}

	/*
		Uses a method similar to the hill climbing
		1- devide all the distances and save the remaining
		2- change the min_distance and do 1 again
		3- end until the error (sum of remainings is minimum)
	*/
	double FindOptimalLength(double min_distance, vector<double> &distances)
	{

		double step = 0.6;

		string param_value="";
 		if(ros::param::get("step_length", param_value)) 
	    	step = strtof(param_value.c_str(), NULL);
		else ROS_WARN("maze_rotation_angle not set, using: %f", step);

		//calculate the new min_distance based tp the min_distance and the step_length (average)
		//min_distance = ( min_distance  + step ) / 2.0;

		int min_distance_in_cm = min_distance * 1000;
		int optimal_length = min_distance_in_cm;
		int size = distances.size();
		int min_length_remaining = std::numeric_limits<int>::max();		

		for(int length = min_distance_in_cm - 100; length < min_distance_in_cm + 200; length = length + 10)
		{
			if(length == 0)
				continue;
			int sum_length_remaining = 0;
			for(int i = 0; i < distances.size(); i++)
			{
				int distance_in_cm = distances.at(i) * 1000;

				int distance_over_length = distance_in_cm / length;
				int distance1 = abs(distance_in_cm - (distance_over_length * length));
				int distance2 = abs(distance_in_cm - ((distance_over_length + 1) * length));
				//int distance3 = abs(distance_in_cm - ((distance_over_length - 1) * length));

				sum_length_remaining += Min(distance1, distance2);
				
			}
			if(sum_length_remaining <= min_length_remaining)
			{
				optimal_length = length;
				min_length_remaining = sum_length_remaining;
			}
			//cout << "sum of remainings to length " << length << " is :" << sum_length_remaining << endl;
		}

		return optimal_length / 1000.0;
	}


	/*
		The main function for calculating the step_length
	*/
	double StepLength()
	{
		double min_distance = std::numeric_limits<double>::max();
		vector<double> distances = DistacesOfClustersLines(min_distance);

		cout << "\nmin distance: " << min_distance << endl;
		//for(int i = 0; i < distances.size(); i++)
		{
			//cout << "distance" << i << ": " << distances.at(i) << endl;
		}

		double step_length = FindOptimalLength(min_distance, distances);
		cout << "step_length: " << step_length << endl;

		return step_length;
	}

	/*
		This method detects the rotation angle of the maze coordinate comparing to the 
		robot's coordinate
	*/
	std_msgs::Float64 CalibrateAngle(State & current, vector<Line> &lines)
	{ 
		double robot_current_angle = current.quaternion.Yaw_in_degrees();
		double angle_threshold = 10.0;
		cluster1.clear();
		cluster2.clear();

		for(int i = 0; i < lines.size(); i++)
		{
			Line line = lines.at(i);
			double line_angle = line.GetAngle();
			//cout << "Line angle: " << line_angle << endl;

			if(first_calibration == 1)
			{
				first_calibration = 2;
				cluster1.Add(line, i);
			}
			else
			{
				if(fabs(line.GetAngle() - cluster1.angle_mean) < angle_threshold)
				{
					cluster1.Add(line, i);
				}
				else if((cluster2.angle_mean == -1000 &&  fabs(line.GetAngle() - cluster1.angle_mean) > (90 - angle_threshold) && fabs(line.GetAngle() - cluster1.angle_mean) < (90 + angle_threshold) ) 
						|| fabs(line.GetAngle() - cluster2.angle_mean) < angle_threshold)
				{
					cluster2.Add(line, i);
				}
				else
				{
					cout << "Error --- line does not belong to any of the line clusters!!!!!!!!!!!" << endl;
				}
			}
		}

		if(first_calibration == 2)
		{
			//specifying north
			
			if(fabs(robot_current_angle - cluster1.angle_mean) < angle_threshold)
			{
				north_south_cluseter = 1;
			}
			else if(fabs(robot_current_angle - cluster2.angle_mean) < angle_threshold)
			{
				north_south_cluseter = 2;
			}
			else
			{
				ROS_ERROR("Robot is not heading to correct direction!!!!!");
				north_south_cluseter = 1;
			}

			first_calibration = 0;
		}

		std_msgs::Float64 mean_angle;

		if(abs(abs(cluster1.angle_mean - cluster2.angle_mean) - 90.0 ) > 20 )
		{
			if(north_south_cluseter == 1)
			{
				cluster2.angle_mean = cluster1.angle_mean + 90.0;
			}
			else
				cluster1.angle_mean = cluster2.angle_mean + 90.0;
			ROS_WARN("One of the detected rotation angles is wrong!");
		}
		if(north_south_cluseter == 1)
			mean_angle.data = (cluster1.angle_mean + (cluster2.angle_mean - 90.0)) / 2.0;
		else
			mean_angle.data = (cluster2.angle_mean + (cluster1.angle_mean - 90.0)) / 2.0;
		
		//just make sure it's less than 90, to make it more roubust
		if (mean_angle.data > 90)
			mean_angle.data = mean_angle.data - 90;
		
		ROS_INFO("mean angle: %f", mean_angle);
		cout << "cluster1 angle mean, size: " << cluster1.angle_mean << ", " << cluster1.size() << endl;
		cout << "cluster2 angle mean, size: " << cluster2.angle_mean << ", " << cluster2.size() << endl;

		return mean_angle;
	}


	/*
		This method gets all the lines in the map, and rotate them 
		with the rotation degree of the maze coordinate
		This function is used in calculating the maze coordinate shift
	*/
	vector<Point> RotateMapLines(vector<Line> &lines, double rotation)
	{	
		vector<Point> rotated_lines;
		for(int i = 0; i < lines.size(); i++)
		{
			Line one_rotated_line;
			RotateFrameBack(lines.at(i).start, one_rotated_line.start);
			RotateFrameBack(lines.at(i).end, one_rotated_line.end);


			rotated_lines.push_back(one_rotated_line.start);
			rotated_lines.push_back(one_rotated_line.end);
			//rotated_lines.push_back(lines.at(i).start);
			//rotated_lines.push_back(lines.at(i).end);
		}
		return rotated_lines;
	}

	//this fucntion detects the sign of the the shift value by looking at the points 
	double ShiftSign(vector<double> & points, double step_length, double shift_value)
	{
		double half_step = step_length / 2.0;
		double sum_of_pluse = 0, sum_of_minus = 0;
		for(int i = 0; i < points.size(); i++)
		{
			double point_plus_shift = points.at(i) + shift_value;
			double point_minus_shif = points.at(i) - shift_value;

			int number_of_steps_plus = point_plus_shift / half_step;
			int number_of_steps_minus = point_minus_shif / half_step;

			double closest_numbers_plus[3]  = { (number_of_steps_plus * half_step),
				((number_of_steps_plus + 1) * half_step), ((number_of_steps_plus - 1) * half_step) };
			double closest_numbers_minus[3]  = { (number_of_steps_minus * half_step),
				((number_of_steps_minus + 1) * half_step), ((number_of_steps_minus - 1) * half_step) };

			int min_shift_index_plus = -1;
			int min_shift_index_minus = -1;
			double shifts_plus[3], abs_shifts_plus[3];
			double shifts_minus[3], abs_shifts_minus[3];

			double min_shift_plus = std::numeric_limits<double>::max();
			double min_shift_minus = std::numeric_limits<double>::max();

			for(int j = 0; j < 3; j++)
			{
				shifts_plus[j] = closest_numbers_plus[j] - point_plus_shift;
				shifts_minus[j] = closest_numbers_minus[j] - point_minus_shif;
				//cout << "shifts " << j << ": " << shifts[j] << endl;
				//calculate abs and see if it's the minimum shift value(what we are looking for)
				abs_shifts_plus[j] = abs(shifts_plus[j]);
				abs_shifts_minus[j] = abs(shifts_minus[j]);

				if(abs_shifts_plus[j] < min_shift_plus)
				{
					min_shift_plus = abs_shifts_plus[j];
					min_shift_index_plus = j;
				}

				if(abs_shifts_minus[j] < min_shift_minus)
				{
					min_shift_minus = abs_shifts_minus[j];
					min_shift_index_minus = j;
				}
			}

			sum_of_pluse += abs_shifts_plus[min_shift_index_plus];
			sum_of_minus+= abs_shifts_minus[min_shift_index_minus];

		}
		
		if(sum_of_pluse > sum_of_minus)
			return -1 * shift_value;
		else
			return shift_value;
	}

	//This method is used for calculating the shift value the maze coordinate comparing to the world coordinate
	//the algorithm is in the Onenote notes (draft - translate coordinates)
	double CalculateShiftValue(vector<double> &points, double step_length)
	{
		double half_step = step_length / 2.0;
		//cout << "half_step: " << half_step << endl;

		double shifts_mean = 0;
		//point the is closer to 0, used to detect the sign
		double min_point =  std::numeric_limits<double>::max();
		for (int i = 0; i < points.size(); ++i)
		{
			//cout << points.at(i) << endl;
			if(points.at(i) < min_point)
			{
				min_point = (points.at(i));
			}

			int number_of_steps = points.at(i) / half_step;
			double closest_numbers[3]  = { (number_of_steps * half_step),
				((number_of_steps + 1) * half_step), ((number_of_steps - 1) * half_step) };

			int min_shift_index = -1;
			double shifts[3], abs_shifts[3];
			double min_shift = std::numeric_limits<double>::max();

			for(int j = 0; j < 3; j++)
			{
				shifts[j] = closest_numbers[j] - points.at(i);
				//cout << "shifts " << j << ": " << shifts[j] << endl;
				//calculate abs and see if it's the minimum shift value(what we are looking for)
				abs_shifts[j] = abs(shifts[j]);
				if(abs_shifts[j] < min_shift)
				{
					min_shift = abs_shifts[j];
					min_shift_index = j;
				}
			}

			//cout << "----min shift: " << min_shift << endl;
			shifts_mean += min_shift;

		}

		shifts_mean = shifts_mean / (double)points.size();

		//detecting the sign

		return ShiftSign(points, step_length, shifts_mean);

	}

	/*
		this method takes in the map lines and the step length
		it calculates the maze coordinate frame shift comparing to the world's coordinate
	*/
	void MazeCorrdinateTransformation(double rotation, double step_length , vector<Line> &lines, double &x_shift, double &y_shift)
	{
		std::vector<Point> rotated_lines = RotateMapLines(lines, rotation);

		std::vector<double> x_values, y_values;

		int size = rotated_lines.size();
		for(int i = 0; i < size; i++)
		{
			x_values.push_back(rotated_lines.at(i).x);
			y_values.push_back(rotated_lines.at(i).y);
		}

		x_shift = CalculateShiftValue(x_values, step_length);
		y_shift = CalculateShiftValue(y_values, step_length);
	}


	/*
		This static method do the rotation to bring a point from the robot's coordinate to the maze coordinate
	*/
	static void RotateFrame(State &goal_location_, State& rotated_goal_location_)
	{
		double theta;
		string param_value="";
	    if(ros::param::get("maze_rotation_angle", param_value)) 
	    	theta = strtof(param_value.c_str(), NULL);
		else ROS_WARN("maze_rotation_angle not set, using: %f", theta);

		//ROS_INFO("maze_coordinate rotation: %f", theta);

		double x = goal_location_.x;
		double y = goal_location_.y;
		//convert theta to radians
		//figure out why * -1???????
		theta = -1 * (M_PI / 180) * theta;
		double new_x = cos(theta) * x + sin(theta) * y;
		double new_y = (-1 * sin(theta) * x) + cos(theta) * y;

		rotated_goal_location_.x = new_x;
		rotated_goal_location_.y = new_y;
	}

 	static void RotateFrame(Point &goal_location_, Point& rotated_goal_location_)
	{
		double theta;
		string param_value="";
	    if(ros::param::get("maze_rotation_angle", param_value)) 
	    	theta = strtof(param_value.c_str(), NULL);
		else ROS_WARN("maze_rotation_angle not set, using: %f", theta);

		//ROS_INFO("maze_coordinate rotation: %f", theta);

		double x = goal_location_.x;
		double y = goal_location_.y;
		//convert theta to radians
		//figure out why * -1???????
		theta = -1 * (M_PI / 180) * theta;
		double new_x = cos(theta) * x + sin(theta) * y;
		double new_y = (-1 * sin(theta) * x) + cos(theta) * y;

		rotated_goal_location_.x = new_x;
		rotated_goal_location_.y = new_y;
	}

	static void RotateFrameBack(Point &goal_location_, Point& rotated_goal_location_)
	{
		double theta;
		string param_value="";
	    if(ros::param::get("maze_rotation_angle", param_value)) 
	    	theta = strtof(param_value.c_str(), NULL);
		else ROS_WARN("maze_rotation_angle not set, using: %f", theta);

		//ROS_INFO("maze_coordinate rotation: %f", theta);

		double x = goal_location_.x;
		double y = goal_location_.y;
		//convert theta to radians
		//figure out why * -1???????
		theta = (M_PI / 180) * theta;
		double new_x = cos(theta) * x + sin(theta) * y;
		double new_y = (-1 * sin(theta) * x) + cos(theta) * y;

		rotated_goal_location_.x = new_x;
		rotated_goal_location_.y = new_y;
	}


	/*
		This function gets a point in the maze coordinate, rotate it, and then transforms it to the map coordinate
	*/
	static void MoveToMazeCoordinate(State &p, State& result)
	{
		double x_shift = 0, y_shift = 0;
		string x_shift_str, y_shift_str;

		if(ros::param::get("x_shift", x_shift_str)) 
	    	x_shift = strtof(x_shift_str.c_str(), NULL);
		else ROS_WARN("x_shifte not set, using: %f", x_shift);

		if(ros::param::get("y_shift", y_shift_str)) 
	    	y_shift = strtof(y_shift_str.c_str(), NULL);
		else ROS_WARN("y_shifte not set, using: %f", y_shift);

		State state_after_rotation;
		Maze_coordinate::RotateFrame(p, state_after_rotation);

		result.x = state_after_rotation.x - x_shift;
		result.y = state_after_rotation.y - y_shift;
	}


	//returns the grid cell in the cell_list variable
	//it can easily be sent to RViz
	void SendGridCellToRviz(double step_length, visualization_msgs::Marker &cellsList )
	{
		const int grid_size = 6;

	    cellsList.header.frame_id = "/map";
	    cellsList.header.stamp = ros::Time::now();
	    cellsList.ns = "points_and_lines";
	    cellsList.action = visualization_msgs::Marker::ADD;
	    cellsList.pose.orientation.w = 1.0;

	    cellsList.id = 0;
	    cellsList.type = visualization_msgs::Marker::LINE_LIST;

	     // LINE_LIST markers use only the x component of scale, for the line width
	    cellsList.scale.x = 0.01;

	    // Line list is red
	    cellsList.color.r = 1.0;
	    cellsList.color.a = 1.0;

	    for(int i = 0; i < grid_size ; i++)
		{	
			for (int j = 0; j < grid_size; j++)
			{
				//geometry_msgs::Point p1, p2, p3, p4;
				State p1, p2, p3, p4;
				p1.x = (i * step_length) + (step_length / 2.0);
				p1.y = (j* step_length) + (step_length / 2.0);
				
				p2.x = (i * step_length) + (step_length / 2.0);
				p2.y = (j* step_length) - (step_length / 2.0);
				
				p3.x = (i * step_length) - (step_length / 2.0);
				p3.y = (j * step_length) - (step_length / 2.0);

				p4.x = (i * step_length) - (step_length / 2.0);
				p4.y = (j* step_length) + (step_length / 2.0);

				State transferred_p1, transferred_p2, transferred_p3, transferred_p4;
				MoveToMazeCoordinate(p1, transferred_p1);
				MoveToMazeCoordinate(p2, transferred_p2);
				MoveToMazeCoordinate(p3, transferred_p3);
				MoveToMazeCoordinate(p4, transferred_p4);

				geometry_msgs::Point pp1, pp2, pp3, pp4;

				pp1.x = transferred_p1.x;
				pp1.y = transferred_p1.y;

				pp2.x = transferred_p2.x;
				pp2.y = transferred_p2.y;

				pp3.x = transferred_p3.x;
				pp3.y = transferred_p3.y;

				pp4.x = transferred_p4.x;
				pp4.y = transferred_p4.y;

				cellsList.points.push_back(pp1);
				cellsList.points.push_back(pp2);
				cellsList.points.push_back(pp2);
				cellsList.points.push_back(pp3);
				cellsList.points.push_back(pp3);
				cellsList.points.push_back(pp4);
				cellsList.points.push_back(pp4);
				cellsList.points.push_back(pp1);
			}
		}
	}



};




#endif
