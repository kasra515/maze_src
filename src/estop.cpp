#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <sstream>
#include <iostream>

#define BUF_SIZE 1024
#define LISTENING_DEPTH 1
#define CONNECTED "CONNECTED"

using namespace std;


int setupSocket(unsigned long serverPort) {
  struct sockaddr_in sock;
  int socket_descriptor;

  if ((socket_descriptor = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket");
    return -1;
  }

  sock.sin_family = AF_INET;
  sock.sin_addr.s_addr = INADDR_ANY;
  sock.sin_port = htons(serverPort);

  if(bind(socket_descriptor, (struct sockaddr*)&sock, sizeof(sock)) == -1) {
    perror("bind");
    return -1;
  }

  if (listen(socket_descriptor, LISTENING_DEPTH) == -1) {
    perror("listen");
    return -1;
  }
  return socket_descriptor;
}

std::string toUpper(const std::string &str) {
  std::string newStr(str);
  std::transform(newStr.begin(), newStr.end(), newStr.begin(), ::toupper);
  return newStr;
}

/**
 * 
 */
int main(int argc, char **argv)
{

  char buffer[BUF_SIZE];
  ros::init(argc, argv, "estop");
  unsigned long serverPort = 33333;

  ros::NodeHandle n;

  ros::Rate loop_rate(10);

  while (ros::ok())
  {
    int current_sd = -1;
    int socket_descriptor = setupSocket(serverPort);

    if((current_sd = accept(socket_descriptor, NULL, 0)) < 0) {
      perror("accept");
      break;
    }
    else 
    {
      ROS_INFO(CONNECTED);
      send(current_sd, CONNECTED, sizeof(CONNECTED), 0);

      while(true)
      {
        //wait for the any key to stop
        if(recv(current_sd, buffer, BUF_SIZE, 0) < 1)
        {
          std::string command(buffer);
          std::string upperCaseCommand = toUpper(command);
          fprintf(stderr, "recv error or got no bytes\n");
          break;
        }

        std::string command(buffer);
        std::string upperCaseCommand = toUpper(command);

        ROS_INFO("Stopped the robot!");
        ros::param::set("estop", "true");

        //wait for the start
        command = "";
        do
        {
          if(recv(current_sd, buffer, BUF_SIZE, 0) < 1)
          {
            command = std::string(buffer);
            std::string upperCaseCommand = toUpper(command);
            fprintf(stderr, "recv error or got no bytes\n");
            break;
          }
          command = std::string(buffer);
          cout << "received command: " << command << endl;
          if(command.compare("start\n") == 0)
          {
            ROS_INFO("Started the robot!");
            ros::param::set("estop", "false");
          }
        }
        while(command.compare("start\n") );
      }
    }


    ros::spinOnce();

    loop_rate.sleep();

    close(current_sd);
  }


  return 0;
}