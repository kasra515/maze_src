#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

char mouseController[] = "Raw Video";
char objWindow[] = "Object Window";
char scribbleWindow[] = "Scribble Window";
char resultWindow[] = "Mouse Controller";

static const std::string OPENCV_WINDOW = "Image window";

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  
public:
  ImageConverter()
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/camera/image_raw", 1, 
      &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/image_converter/output_video", 1);

    cv::namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(OPENCV_WINDOW);
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {

  	bool red_goal = true;
  	string color_param_value="";
    if(ros::param::get("goal_color", color_param_value)) 
    {
    	if(color_param_value.compare ("yellow") == 0 )
    		red_goal = false;
    	else
    		red_goal = true;
    }
	else ROS_WARN("goal_color not set, using RED");

  	static int posX, posY, lastX, lastY; //To hold the X and Y position of tracking color object
	int i, j;

	Mat rgbCameraFrames; //Matrix to hold Raw frames from Webcam in RGB color space
	Mat colorTrackingFrames; //Matrix to hold color filtered Frames in GRAY color space
	Mat resutantFrame; //Matrix to add RAW and user scribble Frames
	Mat threshold;

  	CvMoments colorMoment; //Structure to hold moments information and their order

    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    rgbCameraFrames = cv_ptr->image; //Simply by overloading the operator we copy the Camera frames to our rgbCameraFrames object
 	assert(rgbCameraFrames.type() == CV_8UC3);
 	//Checking whether the color space is in RGB space

  	Mat *scribbleFrame = new Mat(rgbCameraFrames.rows, rgbCameraFrames.cols, CV_8UC3); //A matrix to hold the user scribble on the screen

	GaussianBlur(rgbCameraFrames, colorTrackingFrames, Size(11, 11), 0, 0); //Just a filter to reduce the noise
	//           B  G  R     B   G    R

	if(red_goal)
		inRange(colorTrackingFrames, Scalar(0, 0, 115), Scalar(50, 50, 255), threshold); //We make RED color pixels to white and other colors to black
	else
		inRange(colorTrackingFrames,  Scalar(50, 100, 100), Scalar(80, 255, 255),  threshold);


   	colorMoment = moments(threshold); //We give the binary converted frames for calculating the moments
	//double moment10 = cvGetSpatialMoment(&colorMoment, 1, 0); //Sum of X coordinates of all white pixels
	//double moment01 = cvGetSpatialMoment(&colorMoment, 0, 1); //Sum of Y coordinates of all white pixels
	double area = cvGetCentralMoment(&colorMoment, 0, 0); //Sum of all white color pixels
	//printf("1. x-Axis moments %f  y-Axis moments %f  Area of the moment  %f\n", moment10, moment01, area);
	//printf("2. x movement %f  y movement %f \n\n", moment10 / area, moment01 / area);
//  From terminal :
//  1. x-Axis moments 760645620.000000  y-Axis moments 631169625.000000  Area of the moment  1994355.000000
//  2. x movement 381.399310  y movement 316.478072

   	//lastX = posX;
	//lastY = posY;

   	//posX = (moment10 / area);
	//posY = moment01 / area; //Simple logic which you can understand

   if ((area > 50000)) 
   {
   		//COMMENTED CODE IS FOR SHOWING SEPARATE WINDOWS FOR LIVE IMAGE AND THE DETECTED RED AREA
   		//(*scribbleFrame, cvPoint(posX, posY), cvPoint(lastX, lastY),
     	//cvScalar(0, 255, 255), 1); //To draw a continuous stretch of lines
	   	//line(rgbCameraFrames, cvPoint(posX, posY), cvPoint(lastX, lastY),
 		//cvScalar(0, 255, 255), 5); //To draw a yello point on the center of the colored object
              //cvPoint is used to create a Point data type which holds the pixel location

   		ros::param::set("reached_goal", "yes");
      	
      	if(red_goal)
      	 	ROS_INFO("Red goal has been detected");
      	else
      		ROS_INFO("Yellow goal has been detected");
      	//exit(0);
  	}

   	//imshow(scribbleWindow, *scribbleFrame);
	imshow(mouseController, rgbCameraFrames);
	imshow(objWindow, threshold);
	add(rgbCameraFrames, *scribbleFrame, resutantFrame); //Add two Matrix of the same size
	imshow(resultWindow, resutantFrame);
	waitKey(1); // OpenCV way of adding a delay, generally used to get a Key info.
	

    // Draw an example circle on the video stream
    //if (cv_ptr->image.rows > 60 && cv_ptr->image.cols > 60)
    //  cv::circle(cv_ptr->image, cv::Point(50, 50), 10, CV_RGB(255,0,0));

    // Update GUI Window
    //cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    //cv::waitKey(3);
    
    // Output modified video stream
    //image_pub_.publish(cv_ptr->toImageMsg());
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ImageConverter ic;
  ros::spin();
  return 0;
}