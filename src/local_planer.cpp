
#include "local_planner.h"
#include <math.h>


void Local_Planner::Move_to(State &goal)
{
    
}

int Local_Planner::Is_Heading_to_Goal()
{
    return 0;
}


double Local_Planner::Headding_Angle_error(State& start, State &goal)
{
    double current_state_heading = start.quaternion.Yaw_in_degrees();
    double tan_theta;
    double error = 0;
    //cout << "current_state_heading: " << current_state_heading << endl;
    
    double goal_x_new = goal.x - start.x;
    double goal_y_new = goal.y - start.y;
    
    tan_theta = goal_y_new / goal_x_new;
    double theta = atan(tan_theta);
    
    theta =  (180.0 / M_PI) * theta;
    if(goal_x_new < 0)
    {
        theta += 180;
    }
    
    error = theta - current_state_heading ;
    
    if(error > 180)
        error -= 360;
    else if(error < -180)
        error +=360;

    
    return error;
}



void Local_Planner::turn_toward_goal(double heading_error, double &linear_x, double& angular_z)
{
	double heading_error_abs  = fabs(heading_error);
	linear_x = 0;

	if(heading_error == 0)
	{
		angular_z = 0;
	}
	else if(heading_error > 0)
	{
		//turne left

		if(heading_error_abs > 90)
			angular_z = 0.8;
		else if(heading_error_abs > 60)
			angular_z = 0.5;
		else if(heading_error_abs > 30)
			angular_z = 0.2;
		else if(heading_error_abs > 0)
			angular_z = 0.15;
	}
	else
	{
		//heading_error < 0 => turn right
		if(heading_error_abs > 90)
            angular_z = -0.8;
        else if(heading_error_abs > 60)
            angular_z = -0.5;
        else if(heading_error_abs > 30)
            angular_z = -0.2;
        else if(heading_error_abs > 0)
            angular_z = -0.15;
	}
}

double Local_Planner::Goal_Distance (State &s1, State &goal)
{
	double x1 = s1.x;
	double y1 = s1.y;

	double x2 = goal.x;
	double y2 = goal.y;

	double distance = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2) );

	return distance;
}

void Local_Planner::move_toward_goal(double heading_error, double distance, double &linear_x, double &angular_z)
{
	angular_z = 0;
	linear_x = ( 0.4 * distance) / step_length;
}